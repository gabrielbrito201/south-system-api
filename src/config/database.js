require('dotenv').config()
const mongoose = require('mongoose')
const { success } = require('consola')

const connectDB = async () => {
  await mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })

  success(`MongoDB connected`)
}

module.exports = connectDB
