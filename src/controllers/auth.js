const ErrorResponse = require('../utils/errorResponse')
const User = require('../models/User')

/**
 *
 * @description                 Register a new user
 * @route                       POST /api/v1/register
 * @access                      Public
 *
 */
exports.register = async (req, res, next) => {
  try {
    const user = await User.create({ ...req.body })

    return res.status(201).json({
      success: true,
      data: user,
    })
  } catch (err) {
    return next(err)
  }
}

/**
 *
 * @description                 Login user
 * @route                       POST /api/v1/login
 * @access                      Public
 *
 */
exports.login = async (req, res, next) => {
  try {
    const { email, password } = req.body

    if (!email || !password) {
      return next(
        new ErrorResponse('Please provide an email and password', 400)
      )
    }

    const user = await User.findOne({ email }).select('+password')

    if (!user) {
      return next(new ErrorResponse('Invalid credentials', 401))
    }

    const isMatch = await user.matchPassword(password)

    if (!isMatch) {
      return next(new ErrorResponse('Invalid credentials', 401))
    }

    const token = user.getSignedJwtToken()

    return res.status(200).json({
      success: true,
      data: token,
    })
  } catch (err) {
    return next(err)
  }
}
