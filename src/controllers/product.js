const ErrorResponse = require('../utils/errorResponse')
const Product = require('../models/Product')

/**
 *
 * @description                 Get all products
 * @route                       GET /api/v1/product
 * @access                      Public
 *
 */
exports.getAllProducts = async (req, res, next) => {
  try {
    return res.status(200).json(res.advancedResults)
  } catch (err) {
    return next(err)
  }
}

/**
 *
 * @description                 Get a specif product
 * @route                       GET /api/v1/product/:id
 * @access                      Public
 *
 */
exports.getProduct = async (req, res, next) => {
  try {
    const product = await Product.findById(req.params.id)

    if (!product) {
      return next(
        new ErrorResponse(`Product not found with id of ${req.params.id}`, 404)
      )
    }

    return res.status(200).json({
      success: true,
      data: product,
    })
  } catch (err) {
    return next(err)
  }
}

/**
 *
 * @description                 Create a new product
 * @route                       POST /api/v1/product
 * @access                      Private
 *
 */
exports.createProduct = async (req, res, next) => {
  try {
    const product = await Product.create({ ...req.body })

    return res.status(201).json({
      success: true,
      data: product,
    })
  } catch (err) {
    return next(err)
  }
}

/**
 *
 * @description                 Update a product
 * @route                       PUT /api/v1/product/:id
 * @access                      Private
 *
 */
exports.updateProduct = async (req, res, next) => {
  try {
    let product = await Product.findById(req.params.id)

    if (!product) {
      return next(
        new ErrorResponse(`Product not found with id of ${req.params.id}`, 404)
      )
    }

    product = await Product.findOneAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    })

    return res.status(200).json({
      success: true,
      data: product,
    })
  } catch (err) {
    return next(err)
  }
}

/**
 *
 * @description                 Delete a product
 * @route                       DELETE /api/v1/product/:id
 * @access                      Private
 *
 */
exports.deleteProduct = async (req, res, next) => {
  try {
    const product = await Product.findById(req.params.id)

    if (!product) {
      return next(
        new ErrorResponse(`Product not found with id of ${req.params.id}`, 404)
      )
    }

    product.remove()

    return res.status(200).json({
      success: true,
      data: null,
    })
  } catch (err) {
    return next(err)
  }
}
