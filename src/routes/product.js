const express = require('express')
const {
  createProduct,
  updateProduct,
  deleteProduct,
  getProduct,
  getAllProducts,
} = require('../controllers/product')
const Product = require('../models/Product')

const advancedResults = require('../middlewares/advancedResults')
const { authorize, protect } = require('../middlewares/auth')

const router = express.Router()

router
  .route('/product')
  .get(advancedResults(Product), getAllProducts)
  .post(protect, authorize('manager'), createProduct)
router
  .route('/product/:id')
  .get(getProduct)
  .put(protect, authorize('manager'), updateProduct)
  .delete(protect, authorize('manager'), deleteProduct)

module.exports = router
