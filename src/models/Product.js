const mongoose = require('mongoose')

const ProductSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Please add a name'],
    },
    description: {
      type: String,
      required: [true, 'Please add a description'],
    },
    price: {
      type: String,
      required: [true, 'Please add a price'],
      match: [/(\d+\.\d{1,2})/, 'Please add a valid price'],
    },
  },
  { versionKey: false }
)

module.exports = mongoose.model('Product', ProductSchema)
