const advancedResults = (model) => async (req, res, next) => {
  let query

  const reqQuery = { ...req.query }

  const removeFields = ['page', 'limit']

  removeFields.forEach((param) => delete reqQuery[param])

  const queryStr = JSON.stringify(reqQuery)

  query = model.find(JSON.parse(queryStr))

  const page = parseInt(req.query.page, 10) || 1
  const limit = parseInt(req.query.limit, 10) || 25
  const startIndex = (page - 1) * limit
  const endIndex = page * limit
  const total = await model.countDocuments()

  query = query.skip(startIndex).limit(limit)

  const results = await query

  const pagination = {}

  if (endIndex < total) {
    pagination.next = {
      page: page + 1,
      limit,
    }
  }

  if (startIndex > 0) {
    pagination.prev = {
      page: page - 1,
      limit,
    }
  }

  res.advancedResults = {
    success: true,
    count: results.length,
    pagination,
    data: results,
  }

  next()
}

module.exports = advancedResults
