# South System - API

> Esse projeto é a minha solução ao desafio proposto pela South System.

> OBS: Eu fiz o deploy da aplicação para o Heroku, mas estava sempre retornando 503 nas requisições, e como não terei tempo hábil para resolver esse problema, sugiro que a aplicação seja testada em localhost.

**MongoDB no Docker**

```bash
docker run --name db -p 27017:27017 -d mongo
```

## Passos iniciais

Instalar as dependências do projeto

```bash
npm install
```

Configurar as variáveis de ambiente

```dotenv
# Ambiente
NODE_ENV=

# API
API_VERSION=/api/v1
API_PORT=3000

# MongoDB
MONGO_URI=mongodb://localhost:27017/ssa

# JWT
JWT_SECRET=123n1i23ion98123n123ohi123ih1239812h3o
JWT_EXPIRE=10m
```

Executar o projeto

```bash
npm run dev
```

## Endpoints de produto

> Caso use o Insomnia, basta importar o arquivo "Insomnia-All_2020-11-20" para ter os endpoints.

```json
{
  "Listar todos os produtos": {
    "url": "/api/v1/product",
    "method": "GET"
  },
  "Listar produto": {
    "url": "/api/v1/product",
    "method": "GET"
  },
  "Criar um novo produto": {
    "url": "/api/v1/product/",
    "method": "POST",
    "body": {
      "name": "iPhone 12",
      "description": "Overpriced phone",
      "price": 999.99
    }
  },
  "Atualizar um produto": {
    "url": "/api/v1/product/:id",
    "method": "PUT",
    "body": {
      "name": "iPhone 12 Max",
      "description": "Overpriced phone",
      "price": 1299.99
    }
  },
  "Apagar um produto": {
    "url": "/api/v1/product/:id",
    "method": "DELETE"
  }
}
```

## Endpoints de autenticação

```json
{
  "Registar": {
    "url": "/api/v1/register",
    "method": "POST",
    "body": {
      "name": "User",
      "email": "user@mail.com",
      "role": "manager || user",
      "password": "12345678"
    }
  },
  "Login": {
    "url": "/api/v1/login",
    "method": "POST",
    "body": {
      "email": "user@mail.com",
      "password": "12345678"
    }
  }
}
```

## Paginação

```txt
/api/v1/product?limit=1&page=2
```

## Tipo de usuário

```txt
Role "manager" tem permissão para fazer o CRUD
```
