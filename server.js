require('dotenv').config()
const express = require('express')
const { error, success } = require('consola')

const connectDB = require('./src/config/database')
const errorHandler = require('./src/middlewares/error')
const auth = require('./src/routes/auth')
const product = require('./src/routes/product')

connectDB()

const app = express()

app.use(express.json())
app.use(process.env.API_VERSION, auth)
app.use(process.env.API_VERSION, product)
app.use(errorHandler)

app.listen(process.env.API_PORT, (err) => {
  if (err) error(err)
  success(
    `Server running in ${process.env.NODE_ENV} mode on port ${process.env.API_PORT}`
  )
})
